package org.example;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) throws InterruptedException {


        List<Action> targetList = new ArrayList<>();
        Action targetAlice = new Action("AAPL", 10, 100);
        Action targetAlice2 = new Action("COKE", 20, 390);
        Action targetBob = new Action("AAPL", 10, 140);
        Action targetBob2 = new Action("IBM", 20, 135);
        Action targetCharlie = new Action("COKE", 300, 370);
        targetList.add(targetAlice);
        targetList.add(targetAlice2);
        targetList.add(targetBob);
        targetList.add(targetBob2);
        targetList.add(targetCharlie);
        Costumer costumer = new Costumer("Alice", targetList);
        Costumer costumer2 = new Costumer("Bob", targetList);
        Costumer costumer3 = new Costumer("Charlie", targetList);

        List<Action> actionList = new ArrayList<>();
        Action action = new Action("AAPL", 100, 141);
        Action action1 = new Action("COKE", 1000, 387);
        Action action2 = new Action("IBM", 200, 137);
        actionList.add(action);
        actionList.add(action1);
        actionList.add(action2);

        List<Costumer> costumerList = new ArrayList<>();
        costumerList.add(costumer);
        costumerList.add(costumer2);
        costumerList.add(costumer3);

        ChangePriceThread changePriceThread = new ChangePriceThread(actionList);
        ExchangeThread exchangeThread = new ExchangeThread(actionList, targetList);
        changePriceThread.start();
        exchangeThread.start();

        Thread.sleep(610000);
        System.out.println("The exchange is closed.");
        System.out.println("Final statistic : ");

        System.out.println("The last price for AAPL is : " + actionList.get(actionList.size() - 3).getPrice());
        System.out.println("The last price for COKE is : " + actionList.get(actionList.size() - 2).getPrice());
        System.out.println("The last price for IBM is : " + actionList.get(actionList.size() - 1).getPrice());

        LocalDateTime localDateTime = LocalDateTime.now();
        System.out.println(localDateTime + " Alice bought : " + exchangeThread.getCostumerList().get(0).getAmount() + " actions of : " + exchangeThread.getCostumerList().get(0).getName());
        System.out.println(localDateTime + " Alice bought : " + exchangeThread.getCostumerList().get(1).getAmount() + " actions of : " + exchangeThread.getCostumerList().get(1).getName());
        System.out.println(localDateTime + " Bob bought : " + exchangeThread.getCostumerList().get(2).getAmount() + " actions of : " + exchangeThread.getCostumerList().get(2).getName());
        System.out.println(localDateTime + " Bob bought : " + exchangeThread.getCostumerList().get(3).getAmount() + " actions of : " + exchangeThread.getCostumerList().get(3).getName());
        System.out.println(localDateTime +
                " Charlie bought : " + exchangeThread.getCostumerList().get(4).getAmount() + " actions of : " + exchangeThread.getCostumerList().get(4).getName());


    }

}