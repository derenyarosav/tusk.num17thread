package org.example;

import java.time.LocalDateTime;
import java.util.List;

public class ExchangeThread extends Thread {
    private List<Action> actionList;
    private List<Action> costumerList;


    public ExchangeThread(List<Action> actionList, List<Action> costumerList) {
        this.actionList = actionList;
        this.costumerList = costumerList;

    }


    public void setActionList(List<Action> actionList) {
        this.actionList = actionList;
    }

    public List<Action> getActionList() {
        return actionList;
    }

    public void setCostumerList(List<Action> costumerList) {
        this.costumerList = costumerList;
    }

    public List<Action> getCostumerList() {
        return costumerList;
    }

    public void chaker() {
        LocalDateTime localDateTime = LocalDateTime.now();
        if (costumerList.get(0).getPrice() >= actionList.get(0).getPrice()) {
            getCostumerList().get(0).setAmount(getCostumerList().get(0).getAmount() + 10);
            System.out.println(localDateTime + " Спроба купівлі акції " + actionList.get(0).getName() + " для Alice успішна. Куплено : " + getCostumerList().get(0).getAmount() + " акцій.");

        } else {
            System.out.println(localDateTime + " Спроба купівлі акції " + actionList.get(0).getName() + " для Alice не успішна.");
        }
        if (costumerList.get(1).getPrice() >= actionList.get(1).getPrice()) {
            getCostumerList().get(1).setAmount(getCostumerList().get(1).getAmount() + 20);
            System.out.println(localDateTime + " Спроба купівлі акції " + actionList.get(1).getName() + " для Alice успішна. Куплено : " + getCostumerList().get(1).getAmount() + " акцій.");

        } else {
            System.out.println(localDateTime + " Спроба купівлі акції " + actionList.get(1).getName() + " для Alice не успішна.");
        }
        if (costumerList.get(2).getPrice() >= actionList.get(0).getPrice()) {
            getCostumerList().get(2).setAmount(getCostumerList().get(2).getAmount() + 10);
            System.out.println(localDateTime + " Спроба купівлі акції " + actionList.get(0).getName() + " для Bob успішна. Куплено : " + getCostumerList().get(2).getAmount() + " акцій.");

        } else {
            System.out.println(localDateTime + " Спроба купівлі акції " + actionList.get(0).getName() + " для Bob не успішна.");
        }
        if (costumerList.get(3).getPrice() >= actionList.get(2).getPrice()) {
            getCostumerList().get(3).setAmount(getCostumerList().get(3).getAmount() + 20);
            System.out.println(localDateTime + " Спроба купівлі акції " + actionList.get(2).getName() + " для Bob успішна. Куплено : " + getCostumerList().get(3).getAmount() + " акцій.");

        } else {
            System.out.println(localDateTime + " Спроба купівлі акції " + actionList.get(2).getName() + " для Bob не успішна.");
        }
        if (costumerList.get(4).getPrice() >= actionList.get(1).getPrice()) {
            getCostumerList().get(4).setAmount(getCostumerList().get(4).getAmount() + 300);
            System.out.println(localDateTime + " Спроба купівлі акції " + actionList.get(1).getName() + " для Charlie успішна. Куплено : " + getCostumerList().get(4).getAmount() + " акцій." + "\n");

        } else {
            System.out.println(localDateTime + " Спроба купівлі акції " + actionList.get(1).getName() + " для Charlie не успішна." + "\n");
        }
    }

    @Override
    public void run() {
        for (int i = 0; i <= 120; i++) {
            chaker();

            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }

        }
    }
}
